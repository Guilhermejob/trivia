import { Grid, Paper } from "@material-ui/core";
import { Container } from "./style";

const CardQuestion = (props) => {
  return (
    <Container>
      <div>
        <Grid>
          <Grid xs="12" className="containerCard">
            <Paper elevation={3} className="containerCard">
              <h3>
                Pergunta numero {props.questionNumber}: {props.question} ?
              </h3>
              <h4>Dificuldade: {props.difficulty}</h4>
            </Paper>
          </Grid>
        </Grid>
      </div>
    </Container>
  );
};

export default CardQuestion;
