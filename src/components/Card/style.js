import styled from "styled-components";

export const Container = styled.div`
  /* background-color: red; */

  .MuiGrid-root {
    display: flex;
    align-items: center;
    flex-direction: column;
  }

  .containerCard {
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 20px;
    height: 200px;
  }
`;
