import styled from "styled-components";

export const ContainerHeader = styled.header`
  background: linear-gradient(
    90deg,
    rgba(120, 120, 120, 0.5) 0%,
    rgba(199, 199, 199, 0.5) 80%
  );
  height: 100px;
  width: 100%;
  z-index: 10;
  position: fixed;
  display: flex;
  justify-content: space-between;

  h1 {
    font-family: "Courier New", Courier, monospace;
    margin: 30px;
    a {
      color: black;
    }
  }
`;
