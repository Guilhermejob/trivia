import { ContainerHeader } from "./style";
import { Link } from "react-router-dom";

const Header = () => {
  return (
    <ContainerHeader>
      <h1>
        <Link to="/">Trivia</Link>
      </h1>
      <h1>
        <Link to="/answers">Resultado</Link>
      </h1>
    </ContainerHeader>
  );
};

export default Header;
