import { useState } from "react";
import { useHistory } from "react-router-dom";
import { useQuestions } from "../../Providers/QuestionsProvider";
import { Button } from "@material-ui/core";
import { Container, NoQuestionsContainer } from "./style";

import toast from "react-hot-toast";
import CardQuestion from "../Card";

const CardList = () => {
  const history = useHistory();
  const { questions, getQuestions, checkCorrectAnswer, sendAnswerBolean } =
    useQuestions();
  const [typeQuestion, setTypeQuestion] = useState("");
  const [numberOfQuestions, setNumberOfQuestions] = useState();
  const [next, setNext] = useState(0);
  const [current, setCurrent] = useState(1);

  const nextPage = () => {
    if (next + 1 > questions.length) {
      toast.error("nao a mais perguntas");
    } else {
      setNext(next + 1);
      setCurrent(current + 1);
    }
  };

  const handleSelectChange = (event) => {
    setTypeQuestion(event.target.value);
    console.log(typeQuestion);
  };

  return (
    <Container>
      {questions.length > 0 ? (
        <div className="ContainerCard">
          <CardQuestion
            question={questions[next].question}
            questionNumber={current}
            difficulty={questions[next].difficulty}
          ></CardQuestion>
          {questions[next].type === "boolean" ? (
            <div className="ContainerButtons">
              <Button
                variant="outlined"
                color="primary"
                onClick={() => checkCorrectAnswer(questions[next], "True")}
              >
                Verdadeira
              </Button>
              <Button
                color="secondary"
                variant="outlined"
                onClick={() => checkCorrectAnswer(questions[next], "False")}
              >
                Falsa
              </Button>
            </div>
          ) : (
            <div className="ContainerButtons2">
              {questions[next].incorrect_answers.map((item) => (
                <Button
                  color="primary"
                  variant="outlined"
                  onClick={() => checkCorrectAnswer(questions[next], item)}
                >
                  {item}
                </Button>
              ))}
              <Button
                color="primary"
                variant="outlined"
                onClick={() =>
                  checkCorrectAnswer(
                    questions[next],
                    questions[next].correct_answer
                  )
                }
              >
                {questions[next].correct_answer}
              </Button>
            </div>
          )}

          {next + 2 > questions.length ? (
            <Button
              onClick={sendAnswerBolean}
              color="primary"
              variant="outlined"
            >
              Enviar respostas
            </Button>
          ) : (
            <Button onClick={nextPage} color="primary" variant="outlined">
              Proxima
            </Button>
          )}
        </div>
      ) : (
        <NoQuestionsContainer>
          <h1>Procurar por perguntas</h1>
          <label>Quantidade de perguntas: </label>
          <input
            type="number"
            onChange={(event) => setNumberOfQuestions(event.target.value)}
          />
          <label>Tipo de pergunta: </label>
          <select
            placeholder="tipo"
            value={typeQuestion}
            onChange={handleSelectChange}
          >
            <option selected>None</option>
            <option value="boolean">Verdadeiro ou falso</option>
            <option value="multiple">Multipla escolha</option>
          </select>
          <Button
            className="add"
            color="primary"
            variant="outlined"
            onClick={() => getQuestions(numberOfQuestions, typeQuestion)}
          >
            Add pergunta
          </Button>
          <Button
            className="Cancel"
            color="secondary"
            variant="outlined"
            onClick={() => history.push("/")}
          >
            Cancel
          </Button>
        </NoQuestionsContainer>
      )}
    </Container>
  );
};

export default CardList;
