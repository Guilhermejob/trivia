import styled from "styled-components";

export const Container = styled.div`
  background-color: aliceblue;
  display: flex;
  flex-direction: column;
  margin: auto;
  min-height: 84.2vh;

  .ContainerCard {
    display: flex;
    flex-direction: column;
    align-items: center;

    .ContainerButtons {
      display: flex;
      width: 270px;
      justify-content: space-between;
      margin-bottom: 15px;

      button {
        width: 120px;
      }
    }

    .ContainerButtons2 {
      display: flex;
      width: 800px;
      justify-content: space-between;
      margin-bottom: 15px;
      button {
        width: 150px;
      }
    }
  }
`;

export const NoQuestionsContainer = styled.div`
  /* background-color: green; */
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 50px;
  min-height: 84.2vh;

  input {
    text-align: center;
    width: 190px;
    padding: 5px;
    border: none;
    border-radius: 9px;
    background-color: lightseagreen;
    color: white;
    font-weight: bolder;
    font-size: 18px;
  }
  select {
    width: 200px;
    padding: 5.3px;
    margin-bottom: 20px;
    border-radius: 9px;
    border: none;
    background-color: white;
    outline: none;
    cursor: pointer;
    background-color: lightseagreen;
    color: white;
    font-weight: bolder;
    font-size: 18px;
  }
  option {
  }

  .add {
    background-color: palegreen;
    color: black;
    width: 150px;
    margin-bottom: 15px;
  }
  .Cancel {
    background-color: palevioletred;
    color: white;
    width: 150px;
    :hover {
      color: black;
    }
  }
`;
