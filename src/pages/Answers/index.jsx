import { Container, AnswerContainer } from "./style";
import { useState, useEffect } from "react";
import { Button } from "@material-ui/core";
import toast from "react-hot-toast";

const Answers = () => {
  const [answers, setAnswers] = useState({});
  const [next, setNext] = useState(1);

  useEffect(() => {
    const teste = JSON.parse(localStorage.getItem("Reports"));
    teste ? setAnswers(teste) : console.log("nao tem nada aqui");
  }, []);

  console.log(answers);

  const clear = () => {
    localStorage.clear();
    window.location.reload();
  };

  const proximo = () => {
    next + 2 > answers.length
      ? toast.error("Não tem mais respostas")
      : setNext(next + 1);
  };

  console.log(answers.length, "aaaaaaaaa");
  return (
    <Container>
      <h1>Relatório das respostas</h1>
      <div>
        {answers.length > 0 ? (
          <div>
            <div>
              <AnswerContainer>
                <h3>
                  Pergunta: <span>{answers[next].question}?</span>
                </h3>
                <h4>Resposta certa: {answers[next].correct_answer}</h4>
                <h4>Resposta fornecida: {answers[next].answer}</h4>
                <Button onClick={proximo} color="primary" variant="outlined">
                  Proximo
                </Button>
                <Button onClick={clear} color="secondary" variant="outlined">
                  Limpar
                </Button>
              </AnswerContainer>
            </div>
          </div>
        ) : (
          <div>Não ha relatórios</div>
        )}
      </div>
    </Container>
  );
};

export default Answers;
