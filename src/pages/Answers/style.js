import styled from "styled-components";

export const Container = styled.div`
  margin-top: 150px;
  display: flex;
  flex-direction: column;
  align-items: center;
  font-family: "Courier New", Courier, monospace;
`;

export const AnswerContainer = styled.div`
  border-radius: 15px;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 20px;
  width: 500px;
  height: 300px;
  background-color: #f5deb3;
  box-shadow: 10px 10px 14px -2px rgba(0, 0, 0, 0.75);

  button {
    margin-bottom: 15px;
    width: 100px;
  }
`;
