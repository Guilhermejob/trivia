import CardList from "../../components/CardList";
import { Container } from "./style";

const Questions = () => {
  return (
    <Container>
      <CardList />
    </Container>
  );
};

export default Questions;
