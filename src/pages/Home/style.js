import styled from "styled-components";

export const Container = styled.div`
  margin-top: 150px;
  background-color: #ffe4c4;
  width: 80%;
  min-height: 94.54vh;
  padding: 20px;
  display: flex;
  flex-direction: column;
  align-items: center;

  h1 {
    margin: 0;
    text-align: center;
    font-family: "Courier New", Courier, monospace;
  }
`;

export const ContentContainer = styled.section`
  width: 80%;
  display: flex;
  flex-direction: column;
  font-size: 18px;
  font-family: "Courier New", Courier, monospace;
  align-items: center;

  button.MuiButtonBase-root.MuiButton-root.MuiButton-text.start {
    font-family: "Courier New", Courier, monospace;
    background-color: white;
    border: 3px solid #bc8f8f;
    border-radius: 5px;
    :hover {
      transition: all ease 0.5s;
      filter: brightness(0.8);
    }
  }
`;
