import { useHistory } from "react-router-dom";
import { Container, ContentContainer } from "./style";
import { Button } from "@material-ui/core";

const Home = () => {
  const history = useHistory();

  return (
    <Container>
      <h1>Trivia</h1>
      <ContentContainer>
        <h2>O que é Trivia?</h2>
        <p>
          Traduzido do inglês, Trívia significa curiosidades ou, em outras
          palavras, informações que são consideradas triviais. E o jogo de
          trívia, nada mais é do que o desafio de perguntas e respostas sobre
          essas curiosidades, além disso, podem ser curiosidades sobre filmes,
          séries, desenhos, temas gerais, o que for! Porque o que vale mesmo é
          quem sabe mais! Então, mostre que você é expert nos assuntos mais
          variados, e venha se divertir com a gente.
        </p>
        <Button className="start" onClick={() => history.push("/questions")}>
          Começar a jogar
        </Button>
      </ContentContainer>
    </Container>
  );
};

export default Home;
