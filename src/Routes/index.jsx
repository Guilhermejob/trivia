import Questions from "../pages/Questions";
import Answers from "../pages/Answers";
import Home from "../pages/Home";
import { Switch, Route } from "react-router-dom";

const Routes = () => {
  return (
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route exact path="/questions">
        <Questions />
      </Route>
      <Route exact path="/answers">
        <Answers />
      </Route>
    </Switch>
  );
};

export default Routes;
