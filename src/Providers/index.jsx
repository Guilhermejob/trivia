import { QuestionsProvider } from "./QuestionsProvider";

const Providers = ({ children }) => {
  return <QuestionsProvider>{children}</QuestionsProvider>;
};

export default Providers;
