import api from "../../services/index";
import { useState, useContext, createContext, useEffect } from "react";
import toast from "react-hot-toast";

export const QuestionsContext = createContext();

export const QuestionsProvider = ({ children }) => {
  const [questions, setQuestions] = useState([]);
  const [teste, setTeste] = useState([]);
  const [report, setReport] = useState({});
  const [reportList, setReportList] = useState([]);

  const checkCorrectAnswer = (question, alternative) => {
    question.correct_answer === alternative
      ? toast.success("Parabens, certa resposta", {
          style: {
            border: "1px solid #00881d",
            padding: "16px",
            color: "#ffffff",
            background: "#006d0f",
          },
          iconTheme: {
            primary: "#ffffff",
            secondary: "#00881d",
          },
        })
      : toast.error("Você errou", {
          style: {
            border: "1px solid #ff0000",
            padding: "16px",
            color: "#ffffff",
            background: "#6b0000",
          },
          iconTheme: {
            primary: "#ffffff",
            secondary: "#ff0000",
          },
        });

    setReport({
      question: `${question.question}`,
      correct_answer: `${question.correct_answer}`,
      answer: `${alternative}`,
    });
  };

  useEffect(() => {
    setReportList([...reportList, report]);
  }, [report]);

  const sendAnswerBolean = () => {
    localStorage.setItem("Reports", JSON.stringify(reportList));
  };

  const getQuestions = (numberOfQuestions, type) => {
    api
      .get(`/api.php?amount=${numberOfQuestions}&category=9&type=${type}`)
      .then((res) => setQuestions(res.data.results));
  };

  useEffect(() => {
    console.log(questions);
    questions.forEach((item) => {
      setTeste([...teste, item.correct_answer]);
    });
  }, [questions]);

  console.log(teste);

  return (
    <QuestionsContext.Provider
      value={{
        questions,
        getQuestions,
        report,
        checkCorrectAnswer,
        sendAnswerBolean,
        teste,
      }}
    >
      {children}
    </QuestionsContext.Provider>
  );
};

export const useQuestions = () => useContext(QuestionsContext);
