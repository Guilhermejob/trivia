import Routes from "./Routes/index";
import { Container } from "./style";
import { Toaster } from "react-hot-toast";
import Header from "./components/Header";

const App = () => {
  return (
    <Container>
      <Header />
      <Toaster />
      <Routes />
    </Container>
  );
};

export default App;
