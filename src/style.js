import styled from "styled-components";

export const Container = styled.div`
  min-height: 100vh;
  background-color: #ffffe0;
  display: flex;
  justify-content: center;
`;
